<section class="payment-submit">
    <div class="payment-box">
        <h3>RINCIAN</h3>
        <div class="foto-dokter">
            <img src=<?php echo base_url('Asset/img/81366-middle.png') ?> alt="Dokter">
            <div class="keterangan-dokter">
                <p class="nama-dokter">Dr. Johny Sins</p>
                <p class="jenis-dokter">Fake Doctor</p>
            </div>
        </div>
        <div class="payment-desc">
            <h4>RINGKASAN PEMBAYARAN</h4>
            <div class="biaya-sesi">
                <p>Biaya Dokter</p>
                <p>Rp. 20.000</p>
            </div>
            <div class="bayar-sesi">
                <p>Bayar</p>
                <p>Rp. 20.000</p>
            </div>
        </div>
        <a href="#" class="submit-payment"><button>Pilih Metode Pembayaran</button></a>
    </div>
</section>

<section class="left-description">
    <div class="description-box">
        <h3>Konsultasi dengan dokter yang terpercaya</h3>
        <p>Konsultasi kesehatan membuat anda semakin banyak masalah</p>
        <div class="img-box">
            <img src="<?php echo base_url('Asset/img/81366-middle.png') ?>" alt="">
        </div>
        <p>Manfaat berkonsultasi di web kami :</p>
        <div class="list-manfaat">
            <div class="list-manfaat1">
                <img src="<?php echo base_url('Asset/img/81366-middle.png') ?>" alt="">
                <p>test</p>
            </div>
            <div class="list-manfaat2">
                <img src="<?php echo base_url('Asset/img/81366-middle.png') ?>" alt="">
                <p>test</p>
            </div>
            <div class="list-manfaat3">
                <img src="<?php echo base_url('Asset/img/81366-middle.png') ?>" alt="">
                <p>test</p>
            </div>
        </div>
    </div>
</section>